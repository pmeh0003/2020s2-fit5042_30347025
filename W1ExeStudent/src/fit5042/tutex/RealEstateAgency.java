package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // initialing properties 
    public void createProperty() {
    	Property agencyOne = new Property(1, "24 Boston Ave, Malvern East VIC 3145, Australia", 2 , 150, 420000);
    	Property agencyTwo = new Property(2, "11 Bettina St, Clayton VIC 3168, Australia", 3 , 352, 360000);
    	Property agencyThree = new Property(3, "3 Wattle Ave, Glen Huntly VIC 3163, Australia", 5 , 800, 650000);
    	Property agencyFour = new Property(4, "3 Hamilton St, Bentleigh VIC 3204, Australia", 2 , 170, 435000);
    	Property agencyFive = new Property(5, "82 Spring Rd, Hampton East VIC 3188, Australia", 1 , 60, 820000);
    	try {
    		this.propertyRepository.addProperty(agencyOne);
    		this.propertyRepository.addProperty(agencyTwo);
    		this.propertyRepository.addProperty(agencyThree);
    		this.propertyRepository.addProperty(agencyFour);
    		this.propertyRepository.addProperty(agencyFive); }
    		
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	        
    }
    
    // this method is for displaying all the properties
    public void displayProperties() {
        List<Property> printProperties;
		try {
			printProperties = this.propertyRepository.getAllProperties();
			for(int i = 0 ; i< printProperties.size(); i++) {
	        	
	            System.out.println(printProperties.get(i));
	        }

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
    
    // this method is for searching the property by ID
    public void searchPropertyById() {
        System.out.println("Enter the ID of the property you want to search: ");
        Scanner inputId = new Scanner(System.in);
        int searchId = inputId.nextInt();
        
        try {
        	if (this.propertyRepository.searchPropertyById(searchId)!= null){
        		System.out.println(this.propertyRepository.searchPropertyById(searchId));
        		
        		}
        	else {
        		System.out.println("Prperty with given ID does not exist");
        		
        		}
        	}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void run() {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    // Main method, this will run first
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
            
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
            
        }
    }
}

//Preeti Mehta - 30347025
