/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository{
	
	private ArrayList<Property> agencyProperties = new ArrayList<Property>();

    public SimplePropertyRepositoryImpl() {
    
    	
    }

	@Override
	// this method helps add a new property to the array list
	public void addProperty(Property property) throws Exception {
		// TODO Auto-generated method stub
		this.agencyProperties.add(property);
	}

	@Override
	// this method helps search properties by ID
	public Property searchPropertyById(int id) throws Exception {
		// TODO Auto-generated method stub
		for (int i = 0; i < this.agencyProperties.size(); i++){
			
			if (id == this.agencyProperties.get(i).getId()){
				return this.agencyProperties.get(i);
			}
		}
		return null;
	}

	@Override
	public List<Property> getAllProperties() throws Exception {
		// TODO Auto-generated method stub
		return agencyProperties;
	}
    
}
